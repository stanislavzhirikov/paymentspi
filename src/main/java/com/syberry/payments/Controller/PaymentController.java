package com.syberry.payments.Controller;

import com.syberry.payments.Exception.BadRequestException;
import com.syberry.payments.Service.StripeService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for payments.
 */
@RestController
public class PaymentController {

  @Value("${stripe.secret.key}")
  private String PRIVATE_API_KEY;

  private StripeService stripeService;

  public PaymentController(StripeService stripeService) {
    this.stripeService = stripeService;
  }

  @PostMapping("/create-subscription")
  public @ResponseBody String createSubscription(
          String email,
          String token,
          String plan,
          String coupon
  ) throws BadRequestException {

    if (token == null || plan.isEmpty()) {
      throw new BadRequestException("Token is missing");
    }

    String customerId = stripeService.createCustomer(email, token);

    if (customerId == null) {
      throw new BadRequestException("Unable to create a customer");
    }

    String subscriptionId = stripeService.createSubscription(customerId, plan, coupon);

    if (subscriptionId == null) {
      throw new BadRequestException("Unable to create a subscription");
    }

    return "Success";
  }

  @PostMapping("/cancel-subscription")
  public @ResponseBody String cancelSubscription(String subscriptionId) {
    boolean status = stripeService.cancelSubscription(subscriptionId);
    String message;
    if (status) {
      message = "Subscription cancelled successfully";
    } else {
      message = "Failed to cancel the subscription. Please, try later.";
    }

    return message;
  }

  @PostMapping("/create-charge")
  public @ResponseBody String createCharge(String email, String token) throws BadRequestException {
    if (token == null) {
      throw new BadRequestException("Token is missing");
    }

    String chargeId = stripeService.createCharge(email, token, 5000);

    if (chargeId == null) {
      throw new BadRequestException("An error occurred while creating a charge.");
    }

    return "Your charge Id is : " + chargeId;
  }
}
