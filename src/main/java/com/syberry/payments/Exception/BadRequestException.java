package com.syberry.payments.Exception;

/**
 * Bad Request exception.
 */
public class BadRequestException extends BaseException{

  public BadRequestException(String message) {
    super(message);
  }

  public BadRequestException(Throwable cause) {
    super(cause);
  }

  public BadRequestException(String message, Throwable cause) {
    super(message, cause);
  }
}
