package com.syberry.payments.Service;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Coupon;
import com.stripe.model.Customer;
import com.stripe.model.Subscription;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Service for Stripe component.
 */
@Service
@Controller
public class StripeServiceImpl implements StripeService {

  @Value("${stripe.secret.key}")
  private String PRIVATE_API_KEY;
  @Value("${stripe.public.key}")
  private String PUBLIC_API_KEY;
  @Value("${stripe.currency}")
  private String CURRENCY;

  /**
   * Stripe component constructor
   */
  public StripeServiceImpl() {
  }


  /**
   * Creates customer for payment.
   *
   * @param email customer's Email.
   * @param token Stripe.js token.
   * @return Cusomer identifier.
   */
  @Override
  public String createCustomer(String email, String token) {
    String customerId = null;

    try {
      Stripe.apiKey = PRIVATE_API_KEY;
      Map<String, Object> customerParams = new HashMap<>();
      customerParams.put("description", "Customer for " + email);
      customerParams.put("email", email);

      customerParams.put("source", token); // ^ obtained with Stripe.js
      Customer customer = Customer.create(customerParams);
      customerId = customer.getId();

    } catch (StripeException e) {
      e.printStackTrace();
    }

    return customerId;
  }

  /**
   * Creates subscription for certain customer.
   *
   * @param customerId Customers identifier.
   * @param plan       Subscription plan.
   * @param coupon     Coupon name.
   * @return Subscription identifier.
   */
  @Override
  public String createSubscription(String customerId, String plan, String coupon) {
    String subscriptionId = null;

    try {
      Stripe.apiKey = PRIVATE_API_KEY;
      Map<String, Object> item = new HashMap<>();
      item.put("plan", plan);

      Map<String, Object> items = new HashMap<>();
      items.put("0", item);

      Map<String, Object> params = new HashMap<>();
      params.put("customer", customerId);
      params.put("items", items);

      if (!coupon.isEmpty()) {
        params.put("coupon", coupon);
      }

      Subscription sub = Subscription.create(params);
      subscriptionId = sub.getId();
    } catch (StripeException e) {
      e.printStackTrace();
    }

    return subscriptionId;
  }

  /**
   * Cancel subscription.
   *
   * @param subscriptionId Subscription identifier.
   * @return Boolean status of subscription cancel status.
   */
  @Override
  public boolean cancelSubscription(String subscriptionId) {
    boolean status;
    try {
      Stripe.apiKey = PRIVATE_API_KEY;
      Subscription subscription = Subscription.retrieve(subscriptionId);
      subscription.cancel();
      status = true;

    } catch (StripeException e) {
      e.printStackTrace();
      status = false;
    }
    return status;
  }

  /**
   * Coupon retrieving.
   *
   * @param code Coupon code.
   * @return Coupon.
   */
  @Override
  public Coupon retrieveCoupon(String code) {
    try {
      Stripe.apiKey = PRIVATE_API_KEY;
      return Coupon.retrieve(code);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }

  /**
   * Charge creation.
   *
   * @param email  Customer Email.
   * @param token  Stripe.js token.
   * @param amount Amount in cents.
   * @return Charge identifier.
   */
  @Override
  public String createCharge(String email, String token, long amount) {
    String chargeId = null;
    try {
      Stripe.apiKey = PRIVATE_API_KEY;
      Map<String, Object> chargeParams = new HashMap<>();
      chargeParams.put("amount", amount);
      chargeParams.put("currency", "usd");
      chargeParams.put("description", "Charge for " + email);
      chargeParams.put("source", token); // ^ obtained with Stripe.js

      Charge charge = Charge.create(chargeParams);
      chargeId = charge.getId();
    } catch (StripeException e) {
      e.printStackTrace();
    }

    return chargeId;
  }

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String homepage() {
    return "homepage";
  }

  @RequestMapping(value = "/subscription", method = RequestMethod.GET)
  public String subscriptionPage(Model model) {
    model.addAttribute("stripePublicKey", PUBLIC_API_KEY);
    return "subscription";
  }

  @RequestMapping(value = "/charge", method = RequestMethod.GET)
  public String chargePage(Model model) {
    model.addAttribute("stripePublicKey", PUBLIC_API_KEY);
    return "charge";
  }
}
