package com.syberry.payments.Service;

import com.stripe.model.Coupon;

/**
 * Service for Stripe component.
 */
public interface StripeService {

  /**
   * Creates customer for payment.
   * @param email customer's Email.
   * @param token Stripe.js token.
   * @return Cusomer identifier.
   */
  String createCustomer(String email, String token);

  /**
   * Creates subscription for certain customer.
   * @param customerId Customers identifier.
   * @param plan Subscription plan.
   * @param coupon Coupon name.
   * @return Subscription identifier.
   */
  String createSubscription(String customerId, String plan, String coupon);

  /**
   * Cancel subscription.
   * @param subscriptionId Subscription identifier.
   * @return Boolean status of subscription cancel status.
   */
  boolean cancelSubscription(String subscriptionId);

  /**
   * Coupon retrieving.
   * @param code Coupon code.
   * @return Coupon.
   */
  Coupon retrieveCoupon(String code);

  /**
   * Charge creation.
   * @param email Customer Email.
   * @param token Stripe.js token.
   * @param amount Amount in cents.
   * @return Charge identifier.
   */
  String createCharge(String email, String token, long amount);
}
